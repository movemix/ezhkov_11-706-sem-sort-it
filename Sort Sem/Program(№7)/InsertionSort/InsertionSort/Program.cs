﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Sort
{
    class Program
    {
        static void InsertionSort(List<int> array, ref int countIterInsertion)
        {
            int n = array.Count;
            for (int i = 1; i < n; ++i)
            {
                int key = array[i];
                int j = i - 1;
                while (j >= 0 && array[j] > key)
                {
                    array[j + 1] = array[j];
                    j = j - 1;
                    countIterInsertion++;
                }
                array[j + 1] = key;
                countIterInsertion++;
            }
        }
        static void ShellSort(List<int> array, ref int countIterShell)
        {
            int n = array.Count;
            for (int gap = n / 2; gap > 0; gap /= 2)
            {
                for (int i = gap; i < n; i += 1)
                {
                    int temp = array[i];
                    int j;
                    for (j = i; j >= gap && array[j - gap] > temp; j -= gap)
                    {
                        array[j] = array[j - gap];
                        countIterShell++;
                    }
                    array[j] = temp;
                    countIterShell++;
                }
                countIterShell++;
            }
        }
        static void ShowArray(List<int> array)
        {
            int n = array.Count;
            for (int i = 0; i < n; ++i)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            string path = "D://ezhkov_11-706-sem-sort-it//Sort (Sem)//1.txt";
            int countIterInsertion = 0;
            int countIterShell = 0;
            
            string[] lines = System.IO.File.ReadAllLines(@path);
            List<int> arrayStandart = new List<int>();
            List<int> arrayInsertion = new List<int>();
            List<int> arrayShell = new List<int>();
            foreach (string line in lines)
            {
                arrayStandart.Add(int.Parse(line));
                arrayInsertion.Add(int.Parse(line));
                arrayShell.Add(int.Parse(line));
            }

            Stopwatch stopWatchStandart = new Stopwatch();
            Stopwatch stopWatchInsertion = new Stopwatch();
            Stopwatch stopWatchShell = new Stopwatch();

            stopWatchStandart.Start();
            arrayStandart.Sort();
            stopWatchStandart.Stop();
            TimeSpan tsStandart = stopWatchStandart.Elapsed;
            string elapsedTimeStandart = String.Format("{0:00}:{1:00}:{2:00}",
                tsStandart.Seconds, tsStandart.Milliseconds, tsStandart.Ticks);

            stopWatchInsertion.Start();
            InsertionSort(arrayInsertion, ref countIterInsertion);
            stopWatchInsertion.Stop();
            TimeSpan tsInsertion = stopWatchInsertion.Elapsed;
            string elapsedTimeInsertion = String.Format("{0:00}:{1:00}:{2:00}",
               tsInsertion.Seconds, tsInsertion.Milliseconds, tsInsertion.Ticks);

            stopWatchShell.Start();
            ShellSort(arrayShell, ref countIterShell);
            stopWatchShell.Stop();
            TimeSpan tsShell = stopWatchShell.Elapsed;
            string elapsedTimeShell = String.Format("{0:00}:{1:00}:{2:00}",
                tsShell.Seconds, tsShell.Milliseconds, tsShell.Ticks);

            Console.WriteLine("Стандартная сортировка: ");
            Console.WriteLine("Время работы алгоритма " + elapsedTimeStandart);
            Console.WriteLine();

            Console.WriteLine("Сортировка вставками: ");
            Console.WriteLine("Время работы алгоритма " + elapsedTimeInsertion);
            Console.WriteLine("Количество итераций " + countIterInsertion);
            Console.WriteLine();

            Console.WriteLine("Сортировка Шелла: ");
            Console.WriteLine("Время работы алгоритма " + elapsedTimeShell);
            Console.WriteLine("Количество итераций " + countIterShell);

            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
